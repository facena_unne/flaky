
package unne.facena.tais.flaky.hilos;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Pablo N. Garcia Solanellas
 * 
 */
public class SumaConHilosTest {
    
    /*
    * *********** IMPORTANT*******************
    * Para ejecutar el test. [Alt + F6] en Netbeans
    */

    @Test
    public void test() {
        
        // Probamos  con un arreglo de 100000000 elementos con 10 hilos, esperamos 450000000
        assertEquals(450000000,new SumaConHilos(100000000, 10).sumaEnParalelo());        
        // Probamos  con un arreglo de 100000000 elementos con 10 hilos,
        //assertEquals(450000000,new SumaConHilos(100000000, 10).sumaEnParalelo());
        
    }

    
}
