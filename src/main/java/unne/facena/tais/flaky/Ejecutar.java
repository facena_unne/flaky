/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unne.facena.tais.flaky;

import unne.facena.tais.flaky.hilos.SumaConHilos;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class Ejecutar {
    public static void main(String[] args) {
        
        int _longitud = 10;
        int _hilos = 10;
                
        SumaConHilos at = new SumaConHilos(_longitud, _hilos);         
        
        System.out.println("Longitud: " + _longitud + " sum: " + at.sumaEnParalelo());
    }
}
