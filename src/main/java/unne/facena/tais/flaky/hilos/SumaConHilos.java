/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unne.facena.tais.flaky.hilos;


import java.util.*;
import java.util.concurrent.*;

/**
 *
 * This is a thread example launches multiple threads to
 * sum up all the elements in an array concurrently.
 * Uses worker threads and a semaphore.
 * @author Pablo N. Garcia Solanellas
 */
public class SumaConHilos {
    private int[] array;
    private Semaphore allDone;
    private int numWorkers;
    private List<Worker> workers;
    /**
     * @param p_lenght Longitud del arreglo a poblar 
     * @param p_numWorkers numero de hilos de trabajo
     */    
    public SumaConHilos(int p_lenght, int p_numWorkers) {
        this.setAllDone(new Semaphore(0));
        this.setArray(new int[p_lenght]);
        this.setNumWorkers(p_numWorkers);
        this.setWorkers(new ArrayList<Worker>());
        //Poblamos el arreglo con los valores 0, 1, 2... len-1
        for (int i=0; i<p_lenght; i++) {            
            //resto de la divicion entera
            this.getArray()[i] = i%10; 
        }
    }
    
    /**
     * Este metodo crea y dispara los hilos.
     * @return 
     */
    public long sumaEnParalelo() {
        //reseteamos el semaforo.
        this.setAllDone(new Semaphore(0));
                                
        //calculamos la porcion de trabajo para cara hilo.
        int _porcionDeTrabajo = this.getArray().length / this.getNumWorkers();
        
        for (int i=0; i<this.getNumWorkers(); i++) {
            int start = i * _porcionDeTrabajo;
            int end = (i+1) * _porcionDeTrabajo;
            
            // Caso Especial: 
            //hacemos que el ultimo hilo se lleve el resto del trabajo
            if (i==this.getNumWorkers()-1) end = this.getArray().length;
            
            //Agendamos un nuevo hilo 
            this.addWorker( start, end ).start();     
            
        }
        
        try {
            Thread.sleep(50);           
            //SOLUCION(FIX): Esperamos a que finalicen los hilos
            //allDone.acquire(numWorkers);            
            //o  usando el metodo join() luago de iniciar el hilo.
        } catch (InterruptedException ignored) {}
        
        //acumulador 
        int _resultado = 0;
        // acumulamos el resultado de las operaciones realizadas por los hilos.
        for (Worker w: this.getWorkers()) _resultado += w.getSum();
        
        return _resultado;
    }
    private Worker addWorker(int p_start, int p_end){        
        Worker _worker = new Worker(this, p_start, p_end);
        this.getWorkers().add(_worker);
        return  _worker;
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    public Semaphore getAllDone() {
        return allDone;
    }

    public void setAllDone(Semaphore allDone) {
        this.allDone = allDone;
    }

    public int getNumWorkers() {
        return numWorkers;
    }

    public void setNumWorkers(int numWorkers) {
        this.numWorkers = numWorkers;
    }

    public List<Worker> getWorkers() {
        return workers;
    }

    public void setWorkers(List<Worker> workers) {
        this.workers = workers;
    }

   
} 
