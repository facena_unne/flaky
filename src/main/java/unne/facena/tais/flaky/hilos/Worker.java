package unne.facena.tais.flaky.hilos;

/**
 *
 * @author Pablo N. Garcia Solanellas
 * Worker inner class to add up a section of the array.
 */
public class Worker extends Thread {
    int start;
    int end;
    long sum;    
    SumaConHilos arrayThreading;
    // Note the start and end indexes for this worker
    // in the array. Goes up to but not including end index.
    public Worker(SumaConHilos p_arrayThreading, int start, int end) {
        this.start = start;
        this.end = end;        
        sum = 0;
        this.arrayThreading= p_arrayThreading;
    }
    // Computes the sum for our start..end section
    // in the array (client should call getSum() later).
    public void run() {
        for (int i=start; i<end; i++) {
            sum += arrayThreading.getArray()[i];
        }
        arrayThreading.getAllDone().release();
    }

    public long getSum() {
        return sum;
    }
}
