package unne.facena.tais.flaky.hilos;
/**
 * Investigar porque Si usamos OOWorket en lugar de Worker
 * el test deja de fallar
 * @author Pablo N. Garcia Solanellas
 WorkerOO es:: 
 */
public class WorkerOO extends Thread {
    private int start;
    private int end;
    private long sum;    
    private SumaConHilos elementosParaSumar;
    
    // Note the start and end indexes for this worker
    // in the array. Goes up to but not including end index.
    public WorkerOO(SumaConHilos p_paraSumar, int p_start, int p_end) {
        this.setStart(p_start);
        this.setEnd(p_end);               
        this.setElementosParaSumar(p_paraSumar);
        this.setSum(0);
        
    }
    // Computes the sum for our start..end section
    // in the array (client should call getSum() later).
    public void run() {
        for (int i=this.getStart(); i<this.getEnd(); i++) {
            this.setSum(this.getSum() + this.getElementosParaSumar().getArray()[i]);
        }
        this.getElementosParaSumar().getAllDone().release();
    }

    public long getSum() {
        return sum;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public void setSum(long sum) {
        this.sum = sum;
    }

    public SumaConHilos getElementosParaSumar() {
        return elementosParaSumar;
    }

    public void setElementosParaSumar(SumaConHilos elementosParaSumar) {
        this.elementosParaSumar = elementosParaSumar;
    }

    
    
}
